/* requires:
	jquery.bxslider.min.js
	slick.min.js
*/
$(()=>{

	function linkToParent(i){
		$(i).each(function(index, el) {
			$(el).css('cursor', 'pointer').click(function(e) {
				$( location ).attr("href", $(el).find('a').attr('href').split('#')[0]);
			});
		});
	}

	linkToParent('.item-category');
	//$( ".prev-slide" ).appendTo( ".home-slider .bx-pager.bx-default-pager" );
	//$( ".next-slide" ).appendTo( ".home-slider .bx-pager.bx-default-pager" );

	function clone_svg(){
		$('.home-slider-flechas .bx-controls').clone().appendTo('.home-slider-flechas-wrap');
		// $('.home-slider-flechas .home-slider-prev .bx-prev').clone().appendTo('.home-slider .bx-pager ');
	}

	$('.slider-home').bxSlider({
		captions: true,
		controls: true,
		nextText: '',
		prevText: '',
		nextSelector: '.home-slider-next',
		prevSelector: '.home-slider-prev',
		onSliderLoad: function(currentIndex) {
			$('.slider-home').children().eq(currentIndex).addClass('active')
			clone_svg();
			},
		onSlideBefore: function($slideElement){
			$('.slider-home').children().removeClass('active');
			setTimeout( function(){
				$slideElement.addClass('active');

		},500);
	  }
	});

	$('.slider-clients').bxSlider({
	  controls: true,
	  pager: false,
	  prevText: '',
	  nextText: '',
	  onSliderLoad: function(currentIndex) {
		$('.slider-clients').children().eq(currentIndex).addClass('active')
		},
		onSlideBefore: function($slideElement){
		$('.slider-clients').children().removeClass('active');
		setTimeout( function(){
			$slideElement.addClass('active');
		},500);
	  }
	});

	$('.slider-productos').slick({
		arrows: false,
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 1000,
		speed: 500,
		responsive:[
			{
				breakpoint: 1025,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 481,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});	



	$('.slider-product-pitcher').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		arrows: true,
		prevArrow:'.g-arrow-item-prev',
		nextArrow:'.g-arrow-item-next',
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	
})