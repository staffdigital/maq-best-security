$(()=>{
	var altoScroll = 0
	$(window).scroll(function() {
		altoScroll = $(window).scrollTop();
		if (altoScroll > 1) {
			$('.navbar-brand').addClass('active');
		}
		else{
			$('.navbar-brand').removeClass('active');
		};
	});
	$('.btn-search.dos').click(function() {
		$('.nav-search-responsive').addClass('active');
		$('.bg-nav').addClass('active');
	});
	$('.btn-close-search').click(function() {
		$('.nav-search-responsive').removeClass('active');
		$('.bg-nav').removeClass('active');
	});
	$('.navbar-toggler').click(function() {
		$('.nav-responsive').addClass('active');
		$('.bg-nav').addClass('active');
		$('.navbar').addClass('active');
		$('body').addClass('active');
	});
	$('.btn-close-header').click(function() {
		$('.nav-responsive').removeClass('active');
		$('.bg-nav').removeClass('active');
		$('.navbar').removeClass('active');
		$('body').removeClass('active');
	});
	$('.bg-nav').click(function() {
		$('.nav-responsive').removeClass('active');
		$('.bg-nav').removeClass('active');
		$('.navbar').removeClass('active');
		$('body').removeClass('active');
	});
	var width = $(window).innerWidth();
	if (width < 992) {
		$('.navbar-nav').appendTo('.menu-responsive');
		$('.footer-cliente').appendTo('.menu-responsive');
		$('.footer-herramientas').appendTo('.menu-responsive');
		$('.nav-bar-phone').appendTo('.menu-responsive');
		$('.footer-redes').appendTo('.menu-responsive');
	}
})