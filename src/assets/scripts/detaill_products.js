/* requires:
	jquery.bxslider.min.js
*/
$(()=>{
	// $('.bxslider').bxSlider({
	//   pagerCustom: '#bx-pager',
	//   mode: 'fade',
	//   controls: 'false'
	  
	// });

	$('.nav-tab a').click(function(event){
		event.preventDefault();	

		// capturamos el data-id			
		var bdataid = $(this).attr('data-id');
		$(".nav-tab a").removeClass('active');
		$(this).addClass('active');
		$(".ctn-tab-prin-detaill").removeClass('active');
		$('.ctn-tab-prin-detaill[id="'+bdataid+'"]').addClass('active');
	});


	// Tab carrousel
		$('.tab__carrousel__items__item--js:first-child').addClass('active');
		$('.tab__carrousel__image__big--js:first-child').addClass('active');
		
		var itemSl = $('.tab__carrousel__items__item--js').length;
		if (itemSl > 4) {
			$('.tab__carrousel__items--js').addClass('tab__carrousel--js--on');
			$('.tab__carrousel__items--pitcher').slick({
				autoplay: true,
				autoplaySpeed: 5000,
				arrows: true,
				infinite: true,
				speed: 300,
				slidesToShow: 4,
				slidesToScroll: 1,
				vertical: true,
				verticalSwiping: true,
				responsive: [
					{
						breakpoint: 960,
						settings: {
							vertical:false
						}
					}
				]
			});
		};

		//Carrousel tab slider arrows
		$('.tab__carrousel__items--pitcher').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			console.log(nextSlide);
			setTimeout(function(){
				id = $('.tab__carrousel__items__item--js.slick-current').attr('data-id');				
				$('.tab__carrousel__items__item--js').removeClass('active');
				$('.tab__carrousel__items__item--js.slick-current').addClass('active');
				$('.tab__carrousel__image__big--js').removeClass('active');
				$('.tab__carrousel__image__big--js[id="'+id+'"]').addClass('active');				
			},500);
		});		

		// Carrousel tab items
		$('.tab__carrousel--js .tab__carrousel__items--js .tab__carrousel__items__item--js').click(function(event){
			event.preventDefault();
			var buscaidtab = $(this).attr('data-id');
			$(this).closest('.tab__carrousel--js').find('.tab__carrousel__items--js li').removeClass('active');			
			$(this).addClass('active');
			$(this).closest('.tab__carrousel--js').find('.tab__carrousel__image__big--js').removeClass('active');
			$(this).closest('.tab__carrousel--js').find('.tab__carrousel__image__big--js[id="'+buscaidtab+'"]').addClass('active');
		});


})