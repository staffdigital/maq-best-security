/* requires:
	
*/



$(()=>{
	$('.products .filter-btn-resp').click(function(e) {
		e.preventDefault();
		$('.boxleft-mobile').addClass('active');
		$('body, html').addClass('overflow-hidden');
	});
	$('.products .filter-close').click(function(e) {
		e.preventDefault();
		$('.boxleft-mobile').removeClass('active');
		$('body, html').removeClass('overflow-hidden');
	});	
	if ($(window).width() > 750) {
		// active del acordion
		// $('.list-item').eq(0).find('.list-title').addClass('active');
		var Open_acordion = $('.products .list .list-item.active');
		if (Open_acordion.hasClass('active')) {
			$('.products .list .list-textarea').hide();
			Open_acordion.find('.list-textarea').show();
		};
		// end

		//acordion complemento
		$('.products .list .list-click').click(function(e){
			e.preventDefault();
			if ($(this).parent().hasClass('active')) { // ¿si tiene active?
				$(this).parent().removeClass('active').find('.list-textarea').stop().slideUp(); // remuevo active, subo un nivel, busco, realizo stop y cierro toggle
			}else{
				$('.products .list .list-click').parent().removeClass('active'); // ¿si no tiene active? remuevo active de todos 			
				$('.products .list .list-textarea').stop().slideUp(); // cierro toggle de todos			
				$(this).parent().addClass('active').find('.list-textarea').stop().slideToggle(); // de quien hice click agrego active, busco, realizo stop y abro toogle
			};
		});		
	} else {
		$('.products .boxleft').addClass('boxleft-mobile');
		$('.boxleft-mobile .list .list-click').click(function(e) {
			e.preventDefault();
			$('.boxleft-mobile .list .list-item').removeClass('active')
			$(this).parent().addClass('active');
		});
	}

	// FUNCION FANCY PRODUCTO
	$('.fancy__product__open').click(function(e) {
		e.preventDefault();
		$('.fancy__product').addClass('active');
		$('body, html').addClass('overflow-hidden');
	});
	$('.fancy__product__close, .fancy__prouct__overlay').click(function(e) {
		e.preventDefault();
		$('.fancy__product').removeClass('active');
		$('body, html').removeClass('overflow-hidden');
	});
	// END		
})
